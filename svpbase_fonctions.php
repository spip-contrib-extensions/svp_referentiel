<?php

/**
 * Déclarations de fonctions
 *
 * @plugin SVP pour SPIP
 * @license GPL
 * @package SPIP\SVP\Fonctions
 **/


/**
 * Retourne le nombre d'heures entre chaque actualisation
 * si le cron est activé.
 *
 * @return int
 *     Nombre d'heures (sinon 0)
 **/
function filtre_periode_actualisation_depots() {
	include_spip('genie/svpbase_taches_generales_cron');

	return _SVP_CRON_ACTUALISATION_DEPOTS ? _SVP_PERIODE_ACTUALISATION_DEPOTS : 0;
}

/**
 * Traduit un type de dépot de plugin
 *
 * @param string $type
 *     Type de dépot (svn, git, manuel)
 * @return string
 *     Titre complet et traduit du type de dépot
 **/
function depot_type_traduire($type) {

	$traduction = '';
	if ($type) {
		$traduction = _T('depot:label_type_depot_' . $type);
	}

	return $traduction;
}

/**
 * Renvoie la branche considérée comme la dernière stable.
 *
 * @return string Branche spip actuellement considérée comme la stable courante
 **/
function branche_spip_courante() {

	include_spip('inc/svp_outiller');
	return array_key_last($GLOBALS['infos_branches_spip']);
}

/**
 * Renvoie la branche considérée comme la dernière stable.
 *
 * @return string Branche spip actuellement considérée comme la stable courante
 **/
function compatible_branche_spip_courante($branches) {

	$est_compatible = true;
	if (strpos($branches, branche_spip_courante()) === false) {
		$est_compatible = false;
	}

	return $est_compatible;
}
