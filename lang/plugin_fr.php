<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/svp.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B

	// I
	'info_1_plugin' => '1 plugin',
	'info_aucun_plugin' => 'aucun plugin',
	'info_nb_plugins' => '@nb@ plugins',

	// L
	'label_branches_spip' => 'Compatible',
	'label_plugin_description' => 'Description de la version @version@',
	'label_prefixe' => 'Préfixe',

	// T
	'titre_plugin' => 'Plugin',
	'titre_plugins' => 'Plugins',
	'titre_liste_plugins' => 'Liste des plugins',
	'titre_logo_plugin' => 'Logo du plugin',
);
