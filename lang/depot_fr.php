<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/svp.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_modifier_depot' => 'Modifier le dépôt',

	// I
	'info_1_depot' => '1 dépôt',
	'info_aucun_depot' => 'aucun dépôt',
	'info_nb_depots' => '@nb@ dépôts',

	// L
	'label_type_depot' => 'Type de dépôt :',
	'label_type_depot_git' => 'Dépôt sous GIT',
	'label_type_depot_manuel' => 'Dépôt manuel',
	'label_type_depot_svn' => 'Dépôt sous SVN',
	'label_url_archives' => 'URL du conteneur des archives',
	'label_url_brouteur' => 'URL de la racine des sources',
	'label_url_serveur' => 'URL du serveur',
	'label_xml_depot' => 'Fichier XML du dépôt',

	// T
	'titre_depot' => 'Dépôt',
	'titre_depots' => 'Dépôts',
	'titre_liste_autres_depots' => 'Autres dépôts',
	'titre_liste_depots' => 'Liste des dépôts disponibles',
	'titre_logo_depot' => 'Logo du dépôt',
	'titre_nouveau_depot' => 'Nouveau dépôt',
);
