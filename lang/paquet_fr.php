<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/svp.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_1_paquet' => '1 paquet',
	'info_aucun_paquet' => 'aucun paquet',
	'info_nb_paquets' => '@nb@ paquets',

	// L
	'label_compatibilite_spip' => 'Compatiblité',
	'label_plugin_description' => 'Description de la version @version@',
	'label_version' => 'Version',

	// T
	'titre_paquet' => 'Paquet',
	'titre_paquets' => 'Paquets',
	'titre_liste_paquets_plugin' => 'Liste des paquets du plugin',
);
