<?php
/**
 * Déclarations d'autorisations
 *
 * @plugin SVP Référentiel pour SPIP
 * @license GPL
 **/

/**
 * Fonction du pipeline autoriser. N'a rien à faire.
 *
 * @pipeline autoriser
 */
function svpbase_autoriser() {
}

/**
 * Autoriser la configuration du plugin.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
 */
function autoriser_configurer_svpbase_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre');
}

/**
 * Autoriser l'affichage de la page des dépôts aux admins complets.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
 */
function autoriser_depots_voir_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('defaut');
}

/**
 * Autoriser l'ajout ou l'actualisation d'un dépôt.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
 */
function autoriser_depot_ajouter_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre');
}

/**
 * Autoriser l'édition d'un dépôt.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
 */
function autoriser_depot_modifier_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre');
}

/**
 * Autoriser la suppression d'un dépôt.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
 */
function autoriser_depot_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre');
}

/**
 * Autoriser l'iconification (mettre un logo) d'un dépot.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
 */
function autoriser_depot_iconifier_dist($faire, $type, $id, $qui, $opt) {
	return true;
}
