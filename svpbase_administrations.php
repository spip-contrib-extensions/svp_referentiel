<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin
 *
 * @plugin SVP référentiel pour SPIP
 * @license GPL
 **/

include_spip('base/create');

/**
 * Installation et mises à jour du plugin
 *
 * Crée les tables SQL du plugin (spip_depots, spip_plugins, spip_depots_plugins, spip_paquets)
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 **/
function svpbase_upgrade($nom_meta_base_version, $version_cible) {

	$maj = [];

	$install = ['maj_tables', ['spip_depots', 'spip_plugins', 'spip_depots_plugins', 'spip_paquets']];
	$maj['create'][] = $install;

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Désinstallation du plugin
 *
 * Supprime les tables SQL du plugin (spip_depots, spip_plugins, spip_depots_plugins, spip_paquets)
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 **/
function svpbase_vider_tables($nom_meta_base_version) {

	// Suppression des tables ajoutées par le plugin
	sql_drop_table('spip_depots');
	sql_drop_table('spip_plugins');
	sql_drop_table('spip_depots_plugins');
	sql_drop_table('spip_paquets');

	// Suppression de la meta de configuration du plugin
	effacer_meta('svpbase');

	// Suppression de la meta de version du schéma du plugin
	effacer_meta($nom_meta_base_version);

	spip_log('DESINSTALLATION BDD', 'svpbase.' . _LOG_INFO);
}
