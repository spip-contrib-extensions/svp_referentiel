<?php
/**
 * Utilisations de pipelines
 *
 * @plugin SVP Référentiel pour SPIP
 * @license GPL
 **/

/**
 * Ne pas afficher par défaut les paquets,dépots,plugins locaux dans les boucles
 *
 * On n'affiche dans les boucles (PLUGINS) (DEPOTS) et (PAQUETS)
 * que les éléments distants par défaut (on cache les locaux).
 *
 * Utiliser {tout} pour tout avoir.
 * Utiliser {tout}{id_depot=0} pour avoir les plugins ou paquets locaux.
 *
 * @pipeline pre_boucle
 * @param Boucle $boucle Description de la boucle
 * @return Boucle        Description de la boucle
 **/
function svpbase_pre_boucle($boucle) {

	// DEPOTS, PAQUETS
	// Pour DEPOTS, on n'a jamais id_depot=0 dedans... donc... pas la peine.
	if (
		$boucle->type_requete == 'paquets'
		# OR $boucle->type_requete == 'depots'
	) {
		$id_table = $boucle->id_table;
		$m_id_depot = $id_table . '.id_depot';
		// Restreindre aux depots distants
		if (
			#!isset($boucle->modificateur['criteres']['id_depot']) &&
			!isset($boucle->modificateur['tout'])
		) {
			$boucle->where[] = ["'>'", "'$m_id_depot'", "'\"0\"'"];
		}
	} // PLUGINS
	elseif ($boucle->type_requete == 'plugins') {
		$id_table = $boucle->id_table;
		/*
		// les modificateurs ne se creent que sur les champs de la table principale
		// pas sur une jointure, il faut donc analyser les criteres passes pour
		// savoir si l'un deux est un 'id_depot'...

		$id_depot = false;
		foreach($boucle->criteres as $c){
			if (($c->op == 'id_depot') // {id_depot} ou {id_depot?}
			OR ($c->param[0][0]->texte == 'id_depot')) // {id_depot=x}
			{
				$id_depot = true;
				break;
			}
		}
		*/
		if (
			#	!$id_depot &&
			!isset($boucle->modificateur['tout'])
		) {
			// Restreindre aux plugins distant (id_depot > 0)
			$boucle->from['depots_plugins'] = 'spip_depots_plugins';
			$boucle->where[] = ["'='", "'depots_plugins.id_plugin'", "'$id_table.id_plugin'"];
			$boucle->where[] = ["'>'", "'depots_plugins.id_depot'", "'\"0\"'"];
		}
	}

	return $boucle;
}

/**
 * Enlever les id_ des tables SVP du critère selections conditionnelles,
 * ailleurs que sur les tables de SVP
 *
 * @param array $flux
 * @return array
 */
function svpbase_exclure_id_conditionnel($flux) {
	if (
		!in_array(
			$flux['args']['table'],
			['spip_depots', 'spip_plugins', 'spip_paquets']
		)
	) {
		$flux['data'] = array_merge($flux['data'], ['id_depot', 'id_paquet', 'id_plugin']);
	}
	return $flux;
}
